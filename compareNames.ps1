<#
.SYNOPSIS
    compare names
.DESCRIPTION
    compare names in 2 columns of the csv file
.NOTES
    File Name      : compareNames.ps1
    Author         : gajendra d ambi
    Date           : apr 2017
    Prerequisite   : PowerShell v4+ over win7 and upper.
    Copyright      - None
.LINK
    Script posted over: github.com/MrAmbiG
#>

Write-Host "
A CSV file will be opened (open in excel/spreadsheet)
populate the values,
save & close the file,
Hit Enter to proceed
" -ForegroundColor Blue -BackgroundColor White
$csv = "$PSScriptRoot/addhosts.csv"
get-process | Select-Object ActualName,NamesTobeCompared | Export-Csv -Path $csv -Encoding ASCII -NoTypeInformation
Start-Process $csv
Read-Host "Hit Enter/Return to proceed"

$stopWatch = [system.diagnostics.stopwatch]::startNew()
$stopWatch.Start()

$csv = Import-Csv $csv
foreach ($line in $csv) {
  $name = $($line.ActualName)
  $name1  = $($line.NamesTobeCompared)
  if ($name -ne $name1) { write-host "$name and $name1 are not exactly the same" -BackgroundColor Yellow }
}