function vlanAddAndMapToUCS {
<#
.SYNOPSIS
    adds vlan, vlanID to ucs and map them vnics
.DESCRIPTION
    adds vlan, vlanID to ucs and map them vnics
.NOTES
    File Name      : vlanAddAndMapToUCS.ps1
    Author         : gajendra d ambi
    Date           : Nov 2017
    Prerequisite   : PowerShell v4+, powertool 2.x, win7 or higher
    Copyright      - None
.LINK
    Script posted over: github.com/MrAmbiG
#>
#Start of Script
Connect-Ucs #  connect to ucs

Write-Host "
A CSV file will be opened (open in excel/spreadsheet)
populate the values,
save & close the file,
Hit Enter to proceed
" -ForegroundColor Blue -BackgroundColor White
$csv = "$PSScriptRoot/vlanAddAndMapToUCS.csv"
get-process | Select-Object vlan,vlanID,vnicTemplate | Export-Csv -Path $csv -Encoding ASCII -NoTypeInformation
Start-Process $csv
Read-Host "Hit Enter/Return to proceed"

$csv = Import-Csv $csv
 foreach ($line in $csv) {
  $vlan = $($line.vlan)
  $vlanID  = $($line.vlanID)
  $vnicTemplate  = $($line.vnicTemplate)
  
    write-host -ForeGroundColor yellow  "Add Vlan $vlan $vlanID" 
    Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id $vlan -Name $vlanID
    write-host -ForeGroundColor yellow  "Add Vlan $vlan  $vlanID" 
    Get-UcsOrg -Level root  | Get-UcsVnicTemplate -Name "$vnicTemplate" | Add-UcsVnicInterface -ModifyPresent -DefaultNet "false" -Name "$vlan"
 }
} vlanAddAndMapToUCS
