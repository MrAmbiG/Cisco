﻿function ucsAddVlan {
<#
.SYNOPSIS
    adds vlan to UCS
.DESCRIPTION
    adds vlan to UCS
.NOTES
    File Name      : ucsAddVlan.ps1
    Author         : gajendra d ambi
    Date           : Nov 2017
    Prerequisite   : PowerShell v4+, powertool 2.x, win7 or higher
    Copyright      - None
.LINK
    Script posted over: github.com/MrAmbiG
#>
#Start of Script
Connect-Ucs #  connect to ucs

Write-Host "
A CSV file will be opened (open in excel/spreadsheet)
populate the values,
save & close the file,
Hit Enter to proceed
" -ForegroundColor Blue -BackgroundColor White
$csv = "$PSScriptRoot/ucsAddVlan.csv"
get-process | Select-Object vlan,vlanID | Export-Csv -Path $csv -Encoding ASCII -NoTypeInformation
Start-Process $csv
Read-Host "Hit Enter/Return to proceed"

$csv = Import-Csv $csv
foreach ($line in $csv) {
  $vlan = $($line.vlan)
  $vlanID  = $($line.vlanID)
  
    write-host -ForeGroundColor yellow  "Add Vlan $vlan $vlanID" 
    Get-UcsLanCloud | Add-UcsVlan -DefaultNet no -Id $vlan -Name $vlanID
  }
} ucsAddVlan
